const http = require("http");

const PORT = 8080;
const HOST = "localhost";

const server = http.createServer(function (req, res) {
  console.log("Client connected...");
  res.end("hello from server.");
});

server.listen(PORT, HOST, function (err, done) {
  if (err) {
    console.log("Error in listening: ", err);
  } else {
    console.log(
      "Server started listenting on port 8080: http://" + HOST + ":" + PORT
    );
  }
});
