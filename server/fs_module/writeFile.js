const fs = require("fs");

function writeToFile(pathName, text) {
  return new Promise(function (resolve, reject) {
    fs.writeFile(pathName, text, function (err, done) {
      if (err) {
        reject("Writing failed: " + err);
      } else {
        resolve("Writing success.");
      }
    });
  });
}

module.exports = writeToFile;
