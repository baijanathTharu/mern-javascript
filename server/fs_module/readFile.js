const fs = require("fs");

function readFromFile(pathName) {
  return new Promise(function (resolve, reject) {
    fs.readFile(pathName, "utf8", function (err, done) {
      if (err) {
        reject("Error in reading the file: " + err);
      } else {
        resolve("Content of the file: " + done);
      }
    });
  });
}

module.exports = readFromFile;
