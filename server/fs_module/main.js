const writeToFile = require("./writeFile");
const readFromFile = require("./readFile");

writeToFile("./files/new1.txt", "Testing from new module new.")
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  })
  .finally(function () {
    console.log("Writing to new1 completed...");
  });

readFromFile("./files/new1.txt")
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  })
  .finally(function () {
    console.log("Reading operation completed...");
  });
