const fs = require("fs");

function readFile(fileName) {
  return new Promise(function (resolve, reject) {
    fs.readFile("./files/" + fileName, "utf8", function (err, done) {
      if (err) {
        reject("Reading file failed...");
      } else {
        resolve("Reading success...\nContent of the file:>>\n" + done);
      }
    });
  });
}

module.exports = readFile;
