function urlParameters(str) {
  let strArr = str.substr(1, str.length).split("");
  let paraArr = [];
  strArr.forEach(function (char, idx) {
    if (char !== "/") {
      paraArr.push(char);
    } else {
      paraArr.push(" ");
    }
  });
  paraArr = paraArr.join("").split(" ");
  return paraArr;
}

// console.log(urlParameters("/read/some.txt")); // ['read', 'some.txt']
// console.log(urlParameters("/write/some.txt/iwanttowritesomething.")); // ['write', 'some.txt', 'iwanttowritesomething']

module.exports = urlParameters;
