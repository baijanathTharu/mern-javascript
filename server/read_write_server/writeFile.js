const fs = require("fs");

function writeFile(fileName, data) {
  return new Promise(function (resolve, reject) {
    fs.writeFile("./files/" + fileName, data, function (err, done) {
      if (err) {
        reject("Writing failed..." + err);
      } else {
        resolve("Writing success...");
      }
    });
  });
}

module.exports = writeFile;
