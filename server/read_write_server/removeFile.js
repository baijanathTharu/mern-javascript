const fs = require("fs");

function removeFile(fileName) {
  return new Promise(function (resolve, reject) {
    fs.unlink("./files/" + fileName, function (err, done) {
      if (err) {
        reject("Failed to remove the file. Error: " + err);
      } else {
        resolve("File removed successfully...");
      }
    });
  });
}

module.exports = removeFile;
