// URL Structure:>>>
//  /write/someFile.txt/something to write
// /read/someFile.txt
function processURL(str) {
  let strArr = str.substr(1, str.length).split("");
  let secondSlashIdx = strArr.indexOf("/");
  let lastSlashIdx = strArr.lastIndexOf("/");

  let operation = "";
  let fileName = "";
  let fileContent = "";

  if (secondSlashIdx !== -1) {
    operation = strArr.slice(0, secondSlashIdx).join("");
  }

  if (lastSlashIdx === secondSlashIdx) {
    // case of read operation
    fileName = strArr.slice(secondSlashIdx + 1, strArr.length).join("");
  } else if (lastSlashIdx > secondSlashIdx) {
    // case of write operation
    fileName = strArr.slice(secondSlashIdx, lastSlashIdx).join("");
    fileContent = strArr.slice(lastSlashIdx + 1, strArr.length).join("");
  } else {
    // other operation
    fileName = "";
  }

  return {
    operation,
    fileName,
    fileContent,
  };
}

module.exports = processURL;
