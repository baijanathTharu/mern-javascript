const http = require("http");
const readFile = require("./readFile");
const writeFile = require("./writeFile");
const processURL = require("./processURL");

const PORT = 8080;

const server = http.createServer(function (req, res) {
  let processedURL = processURL(req.url);
  let operation = processedURL.operation;
  let fileName = processedURL.fileName;
  let fileContent = processedURL.fileContent;

  // console.log("Operation: ", operation);
  // console.log("filename: ", fileName);
  // console.log("filecontent: ", fileContent);

  switch (operation) {
    case "read":
      readFile(fileName)
        .then(function (data) {
          res.end(data);
        })
        .catch(function (err) {
          res.end(err);
        });
      break;
    case "write":
      writeFile(fileName, decodeURI(fileContent))
        .then(function (data) {
          res.end(data);
        })
        .catch(function (err) {
          res.end(err);
        });
      break;
    default:
      res.end("Nothing Interesting to do...");
  }
});

server.listen(PORT, function (err, done) {
  if (err) {
    console.log("Listening failed....");
  } else {
    console.log("Listening on port: ", PORT);
  }
});
