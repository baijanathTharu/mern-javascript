const fs = require("fs");

function renameFile(fileName, newName) {
  return new Promise(function (resolve, reject) {
    fs.rename("./files/" + fileName, "./files/" + newName, function (
      err,
      done
    ) {
      if (err) {
        reject("File cannot be renamed. Error: " + err);
      } else {
        resolve("File has beeen renamed...");
      }
    });
  });
}

module.exports = renameFile;
