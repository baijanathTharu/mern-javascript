// using express
const express = require("express");
const PORT = 8080;
const readFile = require("./readFile");
const writeFile = require("./writeFile");
const renameFile = require("./renameFile");
const removeFile = require("./removeFile");

const app = express();

app.get("/", function (req, res) {
  res.end("Nothing to do...");
});

app.get("/read/:fileName", function (req, res) {
  // res.send(req.params);
  const fileName = req.params.fileName;
  readFile(fileName)
    .then(function (data) {
      res.end(data);
    })
    .catch(function (err) {
      res.end(err);
    });
});

app.get("/write/:fileName/:fileContent", function (req, res) {
  const fileName = req.params.fileName;
  const fileContent = decodeURI(req.params.fileContent);

  writeFile(fileName, fileContent)
    .then(function (data) {
      res.end(data);
    })
    .catch(function (err) {
      res.end(err);
    });
});

app.get("/rename/:fileName/:newName", function (req, res) {
  const fileName = req.params.fileName;
  const newName = req.params.newName;

  renameFile(fileName, newName)
    .then(function (data) {
      res.end(data);
    })
    .catch(function (err) {
      res.end(err);
    });
});

app.get("/remove/:fileName", function (req, res) {
  const fileName = req.params.fileName;

  removeFile(fileName)
    .then(function (data) {
      res.end(data);
    })
    .catch(function (err) {
      res.end(err);
    });
});

app.listen(PORT, function (err, done) {
  if (err) {
    console.log("Error listening to server...");
  } else {
    console.log("Listening on PORT: " + PORT);
  }
});
