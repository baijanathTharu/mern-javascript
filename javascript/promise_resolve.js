var fetchEmail = new Promise(function(resolve, reject) {
    setTimeout(function() {
        resolve({
            id: 1,
            email: "test@test.com"
        });

    }, 2000);
});

fetchEmail
    .then(function(data) {
        console.log("Promise is fulfilled: ", data);
    })
    .catch(function(err) {
        console.log("Promise is rejected: ", err);
    })
    .finally(function() {
        console.log("Promise is settled.");
    });