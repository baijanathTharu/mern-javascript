/*
Write a function to group an array of objects by a given property.
*/

function groupBy(source, propertyName) {
    var resObj = {};
    source.forEach(function(item, index) {
        var tempArr = source.filter(function(elm, idx) {
            return item[propertyName] === elm[propertyName];
        });
        resObj[item[propertyName]] = tempArr;
    });

    return resObj;
}

var students = [{
    name: 'Ram',
    class: 3,
    house: 'red',
    status: 'pass'
}, {
    name: 'Shyam',
    class: 4,
    house: 'green',
    status: 'fail'
}, {
    name: 'Hari',
    class: 3,
    house: 'green',
    status: 'pass'
}, {
    name: 'Geeta',
    class: 5,
    house: 'yellow',
    status: 'fail'
}, {
    name: 'Sita',
    class: 3,
    house: 'green',
    status: 'pass'
}, {
    name: 'Rita',
    class: 4,
    house: 'red',
    status: 'fail'
}];

var byClass = groupBy(students, 'class');
var byStatus = groupBy(students, 'status');

console.log('Grouping by class: ', byClass);
console.log('\nGrouping by status: ', byStatus);

var laptops = [{
    name: 'xps',
    brand: 'dell',
    processor: 'i7',
    color: 'grey'
}, {
    name: 'elitebook',
    brand: 'hp',
    processor: 'i5',
    color: 'black'
}, {
    name: 'macbook pro',
    brand: 'apple',
    processor: 'i7',
    color: 'white'
}, {
    name: 'insipiron',
    brand: 'dell',
    processor: 'i5',
    color: 'black'
}, {
    name: 'thinkpad',
    brand: 'lenovo',
    processor: 'i5',
    color: 'black'
}];

var byBrand = groupBy(laptops, 'brand');
console.log('Grouping by brands: ', byBrand);

// streams
// events