function verifyUser(user, callBack) {
    console.log(user, ' wants to log in.');
    console.log('Sending OTP...');
    setTimeout(function() {
        // callBack('OTP incorrect', null);
        callBack(null, 'successfully');
    }, 2000);
}

verifyUser('Ram', function(err, done) {
    if (err) {
        console.log('Error: ', err);
        console.log('Please enter the OTP correctly.');
    } else {
        console.log('OTP received.')
        console.log('You are logged in ', done);
        console.log('Now you can upload files.');
    }
});

console.log('Downloading some images...');
