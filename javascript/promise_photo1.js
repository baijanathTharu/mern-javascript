var takePhoto = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('Photo taken successfully...');
        }, 2000);
    })
}

var editPhoto = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('Photo edited successfully...');
        }, 2000)
    })
}


var uploadPhoto = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            // resolve('Photo uploaded successfully...');
            reject('Error while uploading...');
        }, 2000)
    })
}

// Execution
// first take photo
console.log('Take photo before editing...');

takePhoto()
    .then(function(data) {
        console.log('Data:>> ', data);
        // now edit photo
        editPhoto()
            .then(function(data) {
                console.log('Data:>> ', data);
                // now upload photo
                uploadPhoto()
                    .then(function(data) {
                        console.log('Data:>> ', data);
                    })
                    .catch(function(err) {
                        console.log('Error:>> ', err);
                    })
                    .finally(function() {
                        console.log('Promise for uploading photo is settled.');
                    })
            })
            .catch(function(err) {
                console.log('Error:>> ', err);
            })
            .finally(function() {
                console.log('Promise for editing photo is settled.');
            })
    })
    .catch(function(err) {
        console.log('Error:>> ', err)
    })
    .finally(function() {
        console.log('Promise for taking photo is settled.');
    })

console.log('Performing some non blocking taks....');