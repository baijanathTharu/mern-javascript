function outerFunction() {
  function innerFunction() {
    let text = "I am a closure";
    return text;
  }
  return innerFunction;
}

console.log(outerFunction()());
