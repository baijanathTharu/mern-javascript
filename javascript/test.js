var iAmWeird = "hello";
if (true) {
  var iAmWeird = "I am weird and you can access me from outside this block.";

  const weirdFunction = function () {
    console.log(
      "I am a weird function and you can call me from outside this block"
    );
  };
}

console.log(iAmWeird); // I am weird and you can access me from outside this block.

//weirdFunction(); // I am a weird function and you can call me from outside this block
