function countItemsInArr(arr) {

    var itemsObj = {};

    arr.forEach(function(item, idx) {
        if (!itemsObj[item]) {
            itemsObj[item] = 1;
        } else {
            itemsObj[item]++;
        }

    });

    return itemsObj;
}

function uniqueArr(arr) {
    var countObj = countItemsInArr(arr);
    return Object.keys(countObj);
}

console.log('Items count in array: ', countItemsInArr(['python', 'javascript', 'java', 'c', 'cpp', 'javascript', 'csharp', 'cpp', 'python', 'r', 'r', 'cpp', 'javascript', 'java', 'python', 'csharp']));

console.log('Unique Array: ', uniqueArr(['python', 'javascript', 'java', 'c', 'cpp', 'javascript', 'csharp', 'cpp', 'python', 'r', 'r', 'cpp', 'javascript', 'java', 'python', 'csharp']));