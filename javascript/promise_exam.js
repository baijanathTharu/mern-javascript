// Tasks:::>
// fill form first
// take admission card
// go to exam centre

var fillForm = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('Form is filled.');
        }, 2000);
    })
}

var takeAdmissionCard = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            // reject('Pay the fee first.');
            resolve('Took the admission card.');
        }, 2000);
    })
}

var goToExamCentre = function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('Reached the exam centre.');
        }, 2000);
    })
}

// executions
console.log('Fill form before taking any exam...');

fillForm()
    .then(function(data) {
        console.log('Data: ', data);
        // now take admission card
        takeAdmissionCard()
            .then(function(data) {
                console.log('Data: ', data);
                // now go to exam centre
                goToExamCentre()
                    .then(function(data) {
                        console.log('Data: ', data);
                    })
                    .catch(function(err) {
                        console.log('Error: ', err);
                    })
                    .finally(function() {
                        console.log('Go to exam centre promise is settled...')
                    })
            })
            .catch(function(err) {
                console.log('Error: ', err);
            })
            .finally(function() {
                console.log('Take admission card promise is settled...');
            })
    })
    .catch(function(err) {
        console.log('Error: ', err);
    })
    .finally(function() {
        console.log('Fill form promise is settled...')
    })