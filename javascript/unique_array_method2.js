function allIndexes(arr) {
    var resArr = [];
    arr.forEach(function(item, idx) {
        resArr.push(arr.indexOf(item));
    });
    return resArr;
}

function countItemsInArr(arr) {
    var resObj = {};
    const indexesArr = allIndexes(arr);

    indexesArr.forEach(function(num, idx) {
        if (!resObj[arr[num]]) {
            resObj[arr[num]] = 1;
        } else {
            resObj[arr[num]]++;
        }
    });

    return resObj;
}

function uniqueArr(arr) {
    var resArr = [];
    const indexesArr = allIndexes(arr);
    var uniqueIndexesArr = [];

    indexesArr.forEach(function(num, idx) {
        if (!uniqueIndexesArr.includes(num)) {
            uniqueIndexesArr.push(num);
        }
    });

    uniqueIndexesArr.forEach(function(num, idx) {
        resArr.push(arr[num]);
    });

    return resArr;

}

// console.log('unique Array: ', allIndexes(['python', 'javascript', 'java', 'c', 'cpp', 'javascript', 'csharp', 'cpp', 'python', 'r', 'r', 'cpp', 'javascript', 'java', 'python', 'csharp']));

console.log('Items count in Array: ', countItemsInArr(['python', 'javascript', 'java', 'c', 'cpp', 'javascript', 'csharp', 'cpp', 'python', 'r', 'r', 'cpp', 'javascript', 'java', 'python', 'csharp']));

console.log('unique Array: ', uniqueArr(['python', 'javascript', 'java', 'c', 'cpp', 'javascript', 'csharp', 'cpp', 'python', 'r', 'r', 'cpp', 'javascript', 'java', 'python', 'csharp']));