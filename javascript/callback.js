var canLearnAnyTime = {
    tech1: "HTML",
    tech2: "CSS",
    tech3: "Web Designing",
};

var mustLearnBefore = {
    concept: "Programmming Concept",
    program: "JavaScript",
};

var haveLearned = function(tech) {
    console.log("I have learned ", tech.program, ".");
    console.log("I have learned ", tech.concept, ".");
    console.log("Now I can learn Nodejs.");
};

// haveLearned is a callback
var learnBefore = function(techAnytime, techBefore, haveLearned) {
    console.log(
        "You must learn ",
        techBefore.concept,
        " before learning Nodejs."
    );
    console.log(
        "You must learn ",
        techBefore.program,
        " before learning Nodejs."
    );
    setTimeout(() => {
        haveLearned(mustLearnBefore);
    }, 2000);
    console.log("I am learning ", techAnytime.tech3, ".");
};

console.log("I want to learn Nodejs.");

// learnBefore is a callback
function learnWebDevelopment(learnAnyTime, learnBefore) {
    setTimeout(() => {
        learnBefore(canLearnAnyTime, mustLearnBefore, haveLearned);
    }, 2000);
    console.log("You can learn ", learnAnyTime.tech1, " anytime.");
    console.log("You can learn ", learnAnyTime.tech2, " anytime.");
}

learnWebDevelopment(canLearnAnyTime, learnBefore);

console.log("I am learning HTML.");
console.log("I will also learn CSS.");