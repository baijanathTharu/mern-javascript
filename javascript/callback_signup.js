function signUp(user, callBack) {
    console.log(user, ' wants to sign up.');
    console.log('Sending email for verification.');
    setTimeout(function() {
        callBack(null, 'signed up.');
    }, 2000)
}

signUp('Shyam', function(err, done) {
    if (err) {
        console.log('Please verify your email.');
        console.log('Error: ', err);
    } else {
        console.log('You are ', done);
    }
})

console.log('Downloading some images...');