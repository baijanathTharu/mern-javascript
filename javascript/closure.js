function addition(num1) {
  var addition2 = function (num2) {
    var addition3 = function (num3) {
      return num1 + num2 + num3;
    };
    return addition3;
  };
  return addition2;
}

console.log("3 + 3 + 3 = ", addition(3)(3)(3));
console.log("4 + 5 + 6 = ", addition(4)(5)(6));
