/* Task Part */
function uploadPhoto(photoName, callBack) {
    console.log('Uploading ', photoName, '...');
    setTimeout(function() {
        callBack(null, 'Upload Successful.');
    }, 2000);
}

function editPhoto(photoName, callBack) {
    console.log('Editing ', photoName, '...');
    setTimeout(function() {
        callBack(null, 'Editing Successful.');
    }, 2000)
}

function downloadPhoto(photoName, callBack) {
    console.log('Downloading ', photoName, '...');
    setTimeout(function() {
        // callBack(null, 'Downloading Successful');
        callBack('Downloading failed.', null);
    }, 2000);
}

/* Task Part end */


/* Execution Part */
console.log('Photo Editing Web App');
uploadPhoto('myProfile.png', function(err, done) {
    if (err) {
        console.log('Error: ', err);
    } else {
        console.log('Complete: ', done);
        editPhoto('myProfile.png', function(err, done) {
            if (err) {
                console.log('Error: ', err);
            } else {
                console.log('Complete: ', done);
                downloadPhoto('myProfile.png', function(err, done) {
                    if (err) {
                        console.log('Error: ', err);
                    } else {
                        console.log('Complete: ', done);

                    }
                })
            }
        })
    }
});

console.log('Performing some non blocking tasks');
/* Execution Part end */